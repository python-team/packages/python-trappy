Source: python-trappy
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Javi Merino <vicho@debian.org>,
Build-Depends:
 dh-python,
 python-setuptools (>= 0.6b3),
 python-all (>= 2.6.6-3~),
 debhelper-compat (= 9),
 python-docutils,
 python-sphinx (>= 1.0.7+dfsg-1~),
 python-matplotlib,
 python-tk,
 python-pandas,
 ipython,
Standards-Version: 3.9.6
Homepage: http://arm-software.github.io/trappy
Vcs-Git: https://salsa.debian.org/python-team/packages/python-trappy.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-trappy

Package: python-trappy
Architecture: all
Depends:
 ${misc:Depends},
 ${python:Depends},
Suggests:
 python-trappy-doc,
Description: Trace Analysis and Plotting
 TRAPpy is a framework written in Python for
 analysing and plotting FTrace data by converting it into standardised
 PANDAS DataFrames (tabular times series data representation). The goal is to
 allow developers easy and systematic access to FTrace data and leverage
 the flexibility of PANDAS for the analysis.
 .
 TRAPpy also provides functionality to build complex statistical analysis
 based on the underlying FTrace data.

Package: python-trappy-doc
Architecture: all
Section: doc
Depends:
 ${sphinxdoc:Depends},
 ${misc:Depends},
Description: Trace Analysis and Plotting (documentation)
 TRAPpy is a framework written in Python for analysing and plotting
 FTrace data by converting it into standardised PANDAS DataFrames
 (tabular times series data representation). The goal is to allow
 developers easy and systematic access to FTrace data and leverage the
 flexibility of PANDAS for the analysis.
 .
 TRAPpy also provides functionality to build complex statistical analysis
 based on the underlying FTrace data.
 .
 This is the documentation package.
